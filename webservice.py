from flask import Flask, jsonify, request, redirect, url_for

webservice = Flask(__name__)

# Liste de films stockés en mémoire
films = []

# Fontion de récupération du numéro de page d'un film
def get_film_page(search_query):
    films_per_page = 10
    for index, film in enumerate(films):
        if search_query.isdigit() and film.get('id') == int(search_query):
            return (index // films_per_page) + 1
        elif not search_query.isdigit() and (search_query.lower() in film['nom'].lower() or search_query.lower() in film['description'].lower()):
            return (index // films_per_page) + 1
    return None

# Route de redirection de "/" vers "'/films' à la page 1"
@webservice.route('/')
def index():
    return redirect('/films?page=1')

# Route pour récupérer tous les films
@webservice.route('/films', methods=['GET'])
def get_films():
    page = int(request.args.get('page', 1))
    films_per_page = 10
    start = (page - 1) * films_per_page
    end = start + films_per_page

    paginated_films = films[start:end]

    if paginated_films:
        # Création de la réponse au format JSON HAL
        films_hal = []
        for film in paginated_films:
            film_dict = {
                'id': film['id'],
                'nom': film['nom'],
                'description': film['description'],
                'date_parution': film['date_parution'],
                'note': film['note'],
                'categories': film['categories'],
                '_links': {
                    'self': {'href': url_for('search_film', search_query=film['id'], _external=True)},
                    'collection': {'href': url_for('get_films', _external=True)}
                }
            }
            films_hal.append(film_dict)
        
        response = {
            '_embedded': {
                'films': films_hal
            },
            '_links': {
                'self': {'href': request.url},
                'home': {'href': url_for('index', _external=True)}
            }
        } 
        return jsonify(response)
    else:
        return jsonify({'message': 'Films introuvables !'}), 404

# Route pour récuperer des films par catégorie
@webservice.route('/categories/<category>', methods=['GET'])
def get_films_by_category(category):
    category = category.capitalize()

    # Filtrer les films par catégorie
    films_by_category = [film for film in films if category in film.get('categories', [])]
    
    if films_by_category:
        # Création de la réponse au format JSON HAL
        response = {
            "_embedded": {
                "films": films_by_category
            },
            "_links": {
                "self": {"href": request.url},
                "home": {"href": url_for('index', _external=True)}
            }
        }
        return jsonify(response)
    else:
        return jsonify({'message': 'Aucun film pour cette categorie !'}), 404

# Route pour récupérer un film par son ID / nom / description
@webservice.route('/films/<search_query>', methods=['GET'])
def search_film(search_query):

    # Recherche via ID
    if search_query.isdigit():
        film = next((film for film in films if film.get('id') == int(search_query)), None)
        if film:
            film_page = get_film_page(search_query)
            if film_page:
                film['page'] = film_page
            # Création de la réponse au format JSON HAL
            response = {
                'id': film['id'],
                'nom': film['nom'],
                'description': film['description'],
                'date_parution': film['date_parution'],
                'note': film['note'],
                'categories': film['categories'],
                '_links': {
                    'self': {'href': url_for('search_film', search_query=search_query, _external=True)},
                    'collection': {'href': url_for('get_films', _external=True)}
                }
            }
            return jsonify(response)
        else:
            return jsonify({'message': 'Film introuvable !'}), 404

    # Recherche via nom / description
    else:
        result_films = []
        for film in films:
            if search_query.lower() in film['nom'].lower() or search_query.lower() in film['description'].lower():
                result_films.append(film)

        if result_films:
            first_film_page = get_film_page(str(result_films[0]['id']))
            for film in result_films:
                film['page'] = first_film_page
                film['_links'] = {
                    'self': {'href': url_for('search_film', search_query=film['id'], _external=True)}
                }
            return jsonify(result_films)
        else:
            return jsonify({'message': 'Film introuvable !'}), 404

# Route pour créer un nouveau film
@webservice.route('/films', methods=['POST'])
def create_film():
    data = request.get_json()

    # Vérification des champs obligatoires
    required_fields = ['nom', 'description', 'date_parution', 'categories']
    if not all(field in data for field in required_fields):
        return jsonify({'message': 'Champs obligatoires manquants !'}), 422

    # Vérification de la présence de catégorie(s) sous le format d'une liste
    if not isinstance(data['categories'], list):
        return jsonify({'message': 'La cle "categories" doit etre une liste !'}), 422

    # Création d'un nouveau film et ajout à la liste de films
    new_film = {
        'id': len(films) + 1,
        'nom': data['nom'],
        'description': data['description'],
        'date_parution': data['date_parution'],
        'note': data.get('note', None),
        'categories': data['categories']
    }
    films.append(new_film)
    return jsonify(new_film), 201

# Route pour mettre à jour un film existant
@webservice.route('/films/<int:film_id>', methods=['PUT'])
def update_film(film_id):
    data = request.get_json()

    # Vérification des champs obligatoires
    required_fields = ['nom', 'description', 'date_parution', 'categories']
    if not all(field in data for field in required_fields):
        return jsonify({'message': 'Champs obligatoires manquants'}), 422

    # Vérification de la présence de catégorie(s) sous le format d'une liste
    if not isinstance(data['categories'], list):
        return jsonify({'message': 'La cle "categories" doit etre une liste !'}), 422

    # Recherche du film par son ID
    film = next((film for film in films if film.get('id') == film_id), None)
    if film:
        # Mise à jour des données du film
        film.update(data)
        return jsonify(film), 200
    else:
        return jsonify({'message': 'Film introuvable !'}), 404

# Route pour supprimer un film par son ID
@webservice.route('/films/<int:film_id>', methods=['DELETE'])
def delete_film(film_id):
    global films
    # Suppression du film de la liste de films
    films = [film for film in films if film.get('id') != film_id]
    return jsonify({'message': 'Film supprime !'}), 200

if __name__ == '__main__':
    webservice.run(debug=True)
