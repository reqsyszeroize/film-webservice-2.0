- # WebService Movie

Cette API permet d'ajouter, de récupérer, de mettre à jour & supprimer des films selon la methode CRUD.
Elle comprend des notions de pagination à mesure de 10 éléments par page ainsi que des catégories.
Le format de sortie est en JSON HAL.

## Installation et exécution

- Assurez-vous d'avoir Python installé.
- Installation des dépendances via `pip install Flask`.
- Execution de l'application via `python webservice.py`.

## Routes

### Récupération des films d'une page

- URL : `/films?page=<page_number>`
- Méthode : `GET`
- Description : Cette route renvoie la liste de tous les films enregistrés sur la page spécifiée.

### Récupération des films d'une catégorie

- URL : `/categories/<film_categorie>`
- Méthode : `GET`
- Description : Cette route renvoie la liste de tous les films enregistrés dans la catégorie spécifiée.

### Récupération d'un film spécifique

- URL : `/films/<film_id/film_name/film_description>`
- Méthode : `GET`
- Description : Cette route renvoie les détails d'un film spécifique en utilisant son ID, son nom ou sa description.

### Création d'un nouveau film

- URL : `/films`
- Méthode : `POST`
- Description : Cette route permet d'ajouter un nouveau film à la liste.

- Exemple :  
```shell
curl -X POST http://localhost:5000/films -H "Content-Type: application/json" -d '{
  "nom": "Shrek",
  "description": "Il était une fois, dans un marais lointain",
  "date_parution": "2001-04-07",
  "categories": ["Comedie", "Drame"]
}'
```

### Mise à jour d'un film existant

- URL : `/films/<film_id>`
- Méthode : `PUT`
- Description : Cette route permet de mettre à jour les détails d'un film existant en utilisant son ID.

- Exemple :  

```shell
curl -X POST http://localhost:5000/films -H "Content-Type: application/json" -d '{
  "nom": "Shrek",
  "description": "Il était une fois, dans un marais lointain",
  "date_parution": "2001-04-07",
  "categories": ["Comedie", "Action"]
}'
```
### Suppression d'un film par son ID

- URL : `/films/<film_id>`
- Méthode : `DELETE`
- Description : Cette route permet de supprimer un film spécifique en utilisant son ID.
